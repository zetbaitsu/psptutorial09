package com.example.rest;

import com.example.model.CourseModel;
import com.example.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class CourseRestController {
    @Autowired
    CourseService courseService;

    @RequestMapping("/course/view/{idCourse}")
    public CourseModel view(@PathVariable(value = "idCourse") String idCourse) {
        return courseService.selectCourse(idCourse);
    }

    @RequestMapping("/course/viewall")
    public List<CourseModel> view() {
        return courseService.selectAllCourses();
    }
}
