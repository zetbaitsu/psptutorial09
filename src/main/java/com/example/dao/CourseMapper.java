package com.example.dao;

import com.example.model.CourseModel;
import com.example.model.StudentModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CourseMapper {
    @Select("select id_course, nama, sks from course where id_course = #{id_course}")
    @Results(value = {
            @Result(property = "idCourse", column = "id_course"),
            @Result(property = "nama", column = "nama"),
            @Result(property = "sks", column = "sks"),
            @Result(property = "students", column = "id_course",
                    javaType = List.class,
                    many = @Many(select = "selectStudents"))
    })
    CourseModel selectCourse(@Param("id_course") String idCourse);

    @Select("select id_course, nama, sks from course")
    @Results(value = {
            @Result(property = "idCourse", column = "id_course"),
            @Result(property = "nama", column = "nama"),
            @Result(property = "sks", column = "sks"),
            @Result(property = "students", column = "id_course",
                    javaType = List.class,
                    many = @Many(select = "selectStudents"))
    })
    List<CourseModel> selectAllCourses();

    @Select("select student.npm, name, gpa " +
            "from studentcourse join student " +
            "on studentcourse.npm = student.npm " +
            "where studentcourse.id_course = #{id_course}")
    List<StudentModel> selectStudents(@Param("id_course") String idCourse);
}
