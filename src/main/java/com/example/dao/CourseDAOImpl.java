package com.example.dao;

import com.example.model.CourseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CourseDAOImpl implements CourseDAO {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public CourseModel selectCourse(String idCourse) {
        return restTemplate.getForObject(
                "http://localhost:8080/rest/course/view/" + idCourse,
                CourseModel.class);
    }

    @Override
    public List<CourseModel> selectAllCourses() {
        ResponseEntity<List<CourseModel>> responseEntity =
                restTemplate.exchange("http://localhost:8080/rest/course/viewall",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<CourseModel>>() {});
        return responseEntity.getBody();
    }
}